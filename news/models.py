from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from django.urls import reverse
from mptt.models import MPTTModel, TreeForeignKey

class Category(MPTTModel):

    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
    name = models.CharField(max_length=255, verbose_name='Имя категории')
    slug = models.SlugField(max_length=255, unique=True)
    photo = models.ImageField(blank=True, upload_to='image', null=True, verbose_name='Изображение')
    skin = models.ImageField(blank=True, upload_to='image', null=True, verbose_name='Обложка')

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('category', kwargs={"slug": self.slug})

    class MPTTMeta:
        order_insertion_by = ['name']

    class Meta:
        verbose_name = 'Категория(ю)'
        verbose_name_plural = 'Категории'


class Tag(models.Model):

    BUTTON = (
        ('button', 'Кнопка'),
        ('checkbox', 'Чекбокс'),
        ('text', 'Текст'),
    )

    button = models.CharField(max_length=30, choices=BUTTON, verbose_name='Статус')
    name = models.CharField(max_length=30)
    image = models.ImageField(blank=True,  null=True, verbose_name='Изображение')
    slug = models.SlugField(max_length=255, unique=True)
    skin = models.ImageField(blank=True, upload_to='image', null=True, verbose_name='Обложка')

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('tag', kwargs={"slug": self.slug})

    class Meta:
        verbose_name = 'Тег'
        verbose_name_plural = 'Теги'
        ordering = ['name']

class Post(models.Model):

    STATUS_NEWS = (
        ('Publish', 'Опубликовать'),
        ('Not_to_publish', 'Не публиковать'),
    )

    status = models.CharField(default='P',max_length=30, choices=STATUS_NEWS, verbose_name='Статус')
    title = models.CharField(max_length=255, db_index=True, verbose_name='Наименование')
    slug = models.SlugField(unique=True, verbose_name='Ссылка')
    content = RichTextUploadingField(blank=True, verbose_name='Описание')
    category = TreeForeignKey('Category', verbose_name='Категория', on_delete=models.CASCADE, null=True, blank=True, db_index=True)
    tags = models.ManyToManyField(Tag, verbose_name='Тэг')
    image = models.ImageField(blank=True, upload_to='media/image/', null=True, verbose_name='Изображение')
    views = models.IntegerField(default=0, verbose_name='Количество просмотров')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    update_at = models.DateTimeField(auto_now=True, verbose_name='Дата обновления')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('post', kwargs={"slug": self.slug})

    class Meta:
        verbose_name = 'Статья(ю)'
        verbose_name_plural = 'Статьи'
        ordering = ['-created_at']


class Setting(models.Model):
    skinnews = models.ImageField(blank=True, upload_to='image', null=True, verbose_name='Обложка блога')
    skincat = models.ImageField(blank=True, upload_to='image', null=True, verbose_name='Обложка категории')
    skintag = models.ImageField(blank=True, upload_to='image', null=True, verbose_name='Обложка тега')

    class Meta:
        verbose_name = "Настройка"
        verbose_name_plural = "Настройки"