# Generated by Django 3.1.2 on 2020-12-30 16:19

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0011_setting'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='setting',
            options={'verbose_name': 'Настройка', 'verbose_name_plural': 'Настройки'},
        ),
    ]
