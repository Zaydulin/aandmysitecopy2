# Generated by Django 3.1.2 on 2020-12-24 12:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0009_auto_20201223_1201'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'verbose_name': 'Категория(ю)', 'verbose_name_plural': 'Категории'},
        ),
        migrations.AddField(
            model_name='category',
            name='skin',
            field=models.ImageField(blank=True, null=True, upload_to='image', verbose_name='Обложка'),
        ),
        migrations.AddField(
            model_name='tag',
            name='skin',
            field=models.ImageField(blank=True, null=True, upload_to='image', verbose_name='Обложка'),
        ),
    ]
