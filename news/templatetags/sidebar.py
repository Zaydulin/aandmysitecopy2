from django import template
from django.shortcuts import render

from news.models import Post, Tag, Category

register = template.Library()


@register.inclusion_tag('news/popular_posts_tpl.html')
def get_popular(cnt=3):
    posts = Post.objects.order_by('-views')[:cnt]
    return {"posts": posts}


@register.inclusion_tag('news/tags_tpl.html')
def get_tags():
    tags = Tag.objects.all()
    return {"tags": tags}

@register.inclusion_tag('news/category_tpl.html')
def get_category():
    categorys = Category.objects.all()
    return {"categorys": categorys}

