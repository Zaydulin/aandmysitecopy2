from django.apps import AppConfig


class ProviderConfig(AppConfig):
    name = 'provider'
    verbose_name = 'Поставщик'
