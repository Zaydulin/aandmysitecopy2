from django.contrib import admin
from .models import *


@admin.register(Provider)
class ProviderAdmin(admin.ModelAdmin):
    save_as = True
    list_display = ('name',)


@admin.register(Warehouse)
class WarehouseAdmin(admin.ModelAdmin):
    save_as = True


@admin.register(Warehousemeneger)
class WarehousemenegerAdmin(admin.ModelAdmin):
    save_as = True


admin.site.register(Messenger)
