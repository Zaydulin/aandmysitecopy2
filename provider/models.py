from django.db import models


class Provider(object):
    pass


class Warehousemeneger(models.Model):

    name = models.CharField(max_length=255, verbose_name='имя')
    lastname = models.CharField(max_length=255, verbose_name='Фамилия')
    middlename = models.CharField(max_length=255, verbose_name='Отчество')
    phone = models.CharField(max_length=255, verbose_name='телефон')
    email = models.CharField(max_length=255, verbose_name='E-mail')
    provider = models.ForeignKey('Provider', related_name='Warehousemeneger', verbose_name='Поставщик', blank=True,null=True, on_delete=models.CASCADE)

    def __str__(self):
      return self.name

    class Meta:
        verbose_name = "Менеджер"
        verbose_name_plural = "Менеджеры"


class Warehouse(models.Model):

    name = models.CharField(max_length=255, verbose_name='Регион')
    gorod = models.CharField(max_length=255, verbose_name='Город')
    adress = models.CharField(max_length=255, verbose_name='Адрес')
    provider = models.ForeignKey('Provider', related_name='Warehouse', verbose_name='Поставщик', blank=True,null=True, on_delete=models.CASCADE)


    def __str__(self):
      return self.name

    class Meta:
        verbose_name = "Склад"
        verbose_name_plural = "Склады"


class Provider(models.Model):

    name = models.CharField(max_length=30, verbose_name='Поставащик')
    warehouse = models.ManyToManyField('Warehouse', related_name='Warehouse', verbose_name='Склад')
    warehousemeneger = models.ManyToManyField('Warehousemeneger', related_name='Warehousemeneger', verbose_name='Менеджеры')

    def __str__(self):
      return self.name

    class Meta:
        verbose_name = "Поставщик"
        verbose_name_plural = "Поставщики"


class Messenger(models.Model):

    BUTTON = (
        ('Viber', 'Вайбер'),
        ('WhatsApp', 'Вацап'),
        ('Telegram', 'Телеграм'),
        ('Skype', 'Скайп'),
        ('VK', 'Вконтакте'),
        ('OK', 'Одноклассники'),
        ('Facebook', 'Фейсбук'),
        ('Instagram', 'Инстаграм'),
        ('Twitter', 'Твиттер'),
    )

    button = models.CharField(max_length=30, choices=BUTTON, verbose_name='Тип')
    name = models.CharField(max_length=30, verbose_name='Ссылка')

    def __str__(self):
      return self.name

    class Meta:
        verbose_name = "Связь"
        verbose_name_plural = "Связь"