from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from faicon.fields import FAIconField


class Ikonki(models.Model):
    STATUS = (
        ('Public', 'Страница'),
        ('Group', 'Группа'),
    )
    title = models.CharField(max_length=30, verbose_name='Название')
    icon = FAIconField(verbose_name='Иконка')
    sylka = models.CharField(max_length=30, verbose_name='Сылка')
    status=models.CharField(max_length=10, choices=STATUS, verbose_name='Статус')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Социальная сеть"
        verbose_name_plural = "Социальные сети"

class Setting(models.Model):

    title = models.CharField(max_length=30, verbose_name='Название')
    keyword = models.CharField(max_length=255, verbose_name='Ключевые слова')
    description = models.CharField(max_length=255, verbose_name='Описание')
    company = models.CharField(max_length=255, verbose_name='Компания')
    copyright = models.CharField(max_length=255, verbose_name='copyright')
    favicon = models.ImageField(blank=True, upload_to='images/', verbose_name='Иконка сайта')
    logotip = models.ImageField(blank=True, upload_to='images/', verbose_name='Логотип')
    address = models.CharField(blank=True, max_length=100, verbose_name='Адрес')
    phone = models.CharField(blank=True, max_length=15, verbose_name='Телефон')
    workschedule = models.CharField(blank=True, max_length=15, verbose_name='График работы')
    email = models.CharField(blank=True, max_length=50, verbose_name='Электроная почта')
    smtpserver = models.CharField(blank=True, max_length=50, verbose_name='SMTPserver')
    smtpemail = models.CharField(blank=True, max_length=50, verbose_name='SMTPemail')
    smtppassword = models.CharField(blank=True, max_length=50, verbose_name='SMTPpassword')
    smtpport = models.CharField(blank=True, max_length=8, verbose_name='SMTPport')
    create_at = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    update_at = models.DateTimeField(auto_now=True, verbose_name='Обновлено')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Общие настроики"
        verbose_name_plural = "Общие настроики"


class Slider(models.Model):

    title = models.CharField(max_length=30, verbose_name='Название')
    keyword = models.CharField(max_length=255, verbose_name='Ключевые слова')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Слайдер"
        verbose_name_plural = "Слайдеры"


class Standartpages(models.Model):

    privacypolicy = RichTextUploadingField(blank=True, verbose_name='Политика конфиденциальности')
    conditionofpayment = RichTextUploadingField(blank=True, verbose_name='Условия оплаты')
    usloviyadostavka = RichTextUploadingField(blank=True, verbose_name='Условия Доставка')
    garantii = RichTextUploadingField(blank=True, verbose_name='Гарантии')
    vozvrat = RichTextUploadingField(blank=True, verbose_name='Возврат')
    contact = RichTextUploadingField(blank=True, verbose_name='Контакты')
    aboutus = RichTextUploadingField(blank=True, verbose_name='О нас')
    useragreement = RichTextUploadingField(blank=True, verbose_name='Пользовательское соглашение')

    class Meta:
        verbose_name = "стандартные страницы"
        verbose_name_plural = "стандартные страницы"


class Faq(models.Model):

    problem = models.CharField(max_length=255, verbose_name='Вопрос')
    reply = models.TextField(verbose_name='Ответ')

    def __str__(self):
        return self.problem

    class Meta:
        verbose_name = "ЧаВо"
        verbose_name_plural = "ЧаВо"