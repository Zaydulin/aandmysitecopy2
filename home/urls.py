from django.urls import path

from .views import *

urlpatterns = [
    path('', setting),

    path('faq/', faq),
    path('', slider),
    path('privacypolicy/', privacypolicy),
    path('conditionofpayment/', conditionofpayment),
    path('usloviyadostavka/', usloviyadostavka),
    path('garantii/', garantii),
    path('vozvrat/', vozvrat),
    path('contact/', contact),
    path('aboutus/', aboutus),
    path('useragreement/', useragreement),

]
