from django.contrib import admin

# Register your models here.
from home.models import Setting, Ikonki, Standartpages, Slider, Faq

@admin.register(Ikonki)
class IkonkiAdmin(admin.ModelAdmin):
    save_on_top = True
    list_display = ['title', 'icon']


@admin.register(Setting)
class SettingAdmin(admin.ModelAdmin):
    list_display = ['title','company', 'update_at']
    save_on_top = True
    def has_add_permission(self, request):
        num_objects = self.model.objects.count()
        if num_objects >= 1:
            return False
        else:
            return True

@admin.register(Standartpages)
class StandartpagesAdmin(admin.ModelAdmin):
    save_on_top = True
    def has_add_permission(self, request):
        num_objects = self.model.objects.count()
        if num_objects >= 1:
            return False
        else:
            return True

@admin.register(Faq)
class FaqAdmin(admin.ModelAdmin):
    save_on_top = True



@admin.register(Slider)
class SliderAdmin(admin.ModelAdmin):
    save_on_top = True
    def has_add_permission(self, request):
        num_objects = self.model.objects.count()
        if num_objects >= 1:
            return False
        else:
            return True