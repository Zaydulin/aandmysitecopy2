from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import ListView, DetailView
from .models import Setting, Ikonki, Standartpages, Slider, Faq

from aandmysitecopy2 import settings



#slider
def slider(request):
    slider = Slider.objects.all()
    context = {
        'slider': slider,
        'title': 'Главная'
    }
    return render(request, 'home/home.html', context=context )

#setting
def setting(request):
    setting = Setting.objects.all()
    context = {
        'setting': setting,
        'title': 'Главная'
    }
    return render(request, 'base/base.html', context=context )


#ikonki
def ikonki(request):
    ikonki = Ikonki.objects.get(pk=1)
    context = {
        'ikonki': ikonki,
    }
    return render(request, 'base/header.html', context=context )

#Faq
def faq(request):
    faq = Faq.objects.all()
    context = {
        'faq': faq,
    }
    return render(request, 'home/pages/FAQ.html', context=context )


#privacypolicy
def privacypolicy(request):
    standartpages = Standartpages.objects.get(pk=1)
    context = {
        'standartpages': standartpages,
    }
    return render(request, 'home/pages/privacypolicy.html', context=context )

#conditionofpayment
def conditionofpayment(request):
    standartpages = Standartpages.objects.get(pk=1)
    context = {
        'standartpages': standartpages,
    }
    return render(request, 'home/pages/conditionofpayment.html', context=context )

#usloviyadostavka
def usloviyadostavka(request):
    standartpages = Standartpages.objects.get(pk=1)
    context = {
        'standartpages': standartpages,
    }
    return render(request, 'home/pages/usloviyadostavka.html', context=context )

#privacypolicy
def privacypolicy(request):
    standartpages = Standartpages.objects.get(pk=1)
    context = {
        'standartpages': standartpages,
    }
    return render(request, 'home/pages/privacypolicy.html', context=context )

#garantii
def garantii(request):
    standartpages = Standartpages.objects.get(pk=1)
    context = {
        'standartpages': standartpages,
    }
    return render(request, 'home/pages/garantii.html', context=context )

#vozvrat
def vozvrat(request):
    standartpages = Standartpages.objects.get(pk=1)
    context = {
        'standartpages': standartpages,
    }
    return render(request, 'home/pages/vozvrat.html', context=context )

#contact
def contact(request):
    standartpages = Standartpages.objects.get(pk=1)
    context = {
        'standartpages': standartpages,
    }
    return render(request, 'home/pages/contact.html', context=context )

#aboutus
def aboutus(request):
    standartpages = Standartpages.objects.get(pk=1)
    context = {
        'standartpages': standartpages,
    }
    return render(request, 'home/pages/aboutus.html', context=context )

#useragreement
def useragreement(request):
    standartpages = Standartpages.objects.get(pk=1)
    context = {
        'standartpages': standartpages,
    }
    return render(request, 'home/pages/useragreement.html', context=context )



