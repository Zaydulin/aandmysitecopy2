from django.contrib import admin

from .models import *

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    change_form_template = 'shop/custom_admin/change_form.html'
    #exclude = ('features',)




@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    save_as = True
    save_on_top = True
    list_display = ('id', 'name',)
    prepopulated_fields = {'slug': ('name',)}
    list_display_links = ('id', 'name')
    search_fields = ('name',)


@admin.register(CartProduct)
class CartProductAdmin(admin.ModelAdmin):
    save_as = True
    save_on_top = True

@admin.register(Cart)
class Cartdmin(admin.ModelAdmin):
    save_as = True
    save_on_top = True


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    save_as = True
    save_on_top = True


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    save_as = True
    save_on_top = True



@admin.register(Setting)
class SettingPageAdmin(admin.ModelAdmin):
    save_on_top = True
    fieldsets = (
        (None, {
            'fields': ('skinshop', 'skinshopcat', 'skinshopprod')
        }),
        ('Платежная система сбербанка', {
            'fields': ('sber', 'sberlogin', 'sberpass'),

        }),
        ('Платежная система яндекса', {
            'fields': ('yand', 'yanlogin', 'yanpass'),

        }),
        ('Платежная система Альфабанка', {
            'fields': ('alfa', 'allogin', 'alpass'),
        }),
        ('Доставка СДЭК', {
            'fields': ('cdek', 'cdeklogin', 'cdekpass'),
        }),
        ('Доставка Почта РОССИИ', {
            'fields': ('pochros', 'prlogin', 'prpass'),
        }),
    )


    def has_add_permission(self, request):
        num_objects = self.model.objects.count()
        if num_objects >= 1:
            return False
        else:
            return True