**приложение HOME**
1) вывести все данные в шаблоне
2) вывести иконки из модели Ikonki в шаблон templates/base/header.html (47 строка) и templates/base/footer.html (22 строка)

**приложение news**

1) Вывести фотографию категории из модели skin в Шаблон templates/news/news_category.html
2) Вывести категории по иерархие в templates/news/news_category.html - templates/news/news.html - templates/news/news_tag.html (130 строка)

**приложение shop**

1) Вывести фотографию категории из модели skin в Шаблон templates/news/news_category.html
2) Вывести категории по иерархие в templates/shop/product_category.html - templates/shop/product.html - templates/shop/product_tag.html (130 строка)
3) Вывести теги на странице товара
Все приложения 

**APPS**

- [ ] SHOP - магазин
- [x] NEWS - новости
- [ ] SMS - смс
- [ ] TELEPHONY - телефония
- [ ] SOCIAL NETWORK - социальные сети
- [ ] MESSENGER - мессенджер
- [ ] E-MAIL - электронная почта
- [ ] HOME - базовая
- [ ] AFFILIATEPROGRAM - партнёрка
- [ ] PROVIDER - поставщик
- [ ] ADVERTISING - реклама

**Модели**

- [ ] Товары(категории и вариации)
- [ ] Новости(категории и теги)
- [ ] Настройки (социальные сети, описание, название, smpt,страницы, Контакты)
- [ ] Склад(адреса складов. Точка отправки при заказе)
- [ ] Пользователь
- [ ] Заказ
- [ ] Доставка
- [ ] Оплата
- [ ] Сравнение
- [ ] Корзина
- [ ] Импорт и экспорт
- [ ] Комментарии
- [ ] Оценка
- [ ] Баллы
- [ ] Реферальная программа

**Шаблоны**

- [ ] Пользователь
- [ ] Заказ
- [ ] Доставка
- [ ] Оплата
- [ ] Сравнение
- [ ] Корзина
- [ ] Админка

**ДОПОЛНИТЕЛЬНЫЕ МОДЕЛИ**

Сотрудники

- [ ] Список сотрудников
- [ ] Задачи
- [ ] Чат с ними

Клиенты

- [ ] Список клиентов

Поставщики

- [ ] Список поставщиков

Zadarma

- [ ] Подключение номера
- [ ] Запись звонков
- [ ] Звонки
- [ ] Отправка смс.
- [ ] Баланс

Vk

- [ ] Чат переписка
- [ ] Аккаунты
- [ ] Чат бот
- [ ] Баланс вк таргет
- [ ] Авто-Постер

 Odnoklassniki'

- [ ] Чат переписка
- [ ] Чат бот

Twitter

Instagram

Facebook

Watshapp

Viber

Telegram

Email

1