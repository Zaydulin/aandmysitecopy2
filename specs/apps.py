from django.apps import AppConfig


class SpecsConfig(AppConfig):
    name = 'specs'
    verbose_name = 'Характеристики товара'
    verbose_name_plural = 'Характеристики товаров'
